const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const mongoose = require("mongoose");
const config = require("./config");
const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(cors({origin: '*'}));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.options('*', cors());

app.use('/', require('./routes'));
app.use('/api/oauth/token', require('./routes/auth'));
app.use('/api/v1/users', require('./routes/users'));

mongoose.connect(config.dbURL)
    .then(() => {
        app.listen(process.env.PORT || config.PORT, () => {
            console.log(`Server Started on port ${process.env.PORT || config.PORT}`);
        });
    })
    .catch((err) => {
        console.error(`Error connecting to the database. n${err}`);
    })



