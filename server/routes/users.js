const express = require('express');
const authenticate = require("../middleware/authenticate")
const users = require("../database/usersModel");
const mongoose = require("mongoose");
const router = express.Router();

/* GET users listing. */
router.get('/', authenticate, async (req, res, next) => {
    try {
        const query = {
            user_role: /USER/
        }
        req.query.email ? query["email"] = { $regex: '.*' + req.query.email + '.*', $options: "i" } : null
        req.query.first_name ? query["first_name"] = { $regex: '.*' + req.query.first_name + '.*', $options: "i" }  : null
        req.query.last_name ? query["last_name"] = { $regex: '.*' + req.query.last_name + '.*', $options: "i" }  : null

        let usersAll
        if (req.query.page) {
            usersAll = await users.paginate(query, {page: req.query.page, limit: 10})
        } else {
            usersAll = await users.find()
        }

        usersAll = JSON.parse(JSON.stringify(usersAll))
        res.send({
            success: true,
            body: usersAll
        })
    } catch (e) {
        res.send({
            success: false,
            msg: 'Something went wrong!'
        });
    }
});

/* SAVE users listing. */
router.post('/', authenticate, async (req, res, next) => {
    try {
        const {id, email, first_name, last_name, password, newEmail} = req.body
        if (id) {
            if (!mongoose.Types.ObjectId.isValid(id)) {
                return res.status(400).json({
                    success: false,
                    msg: 'Unable to find User!'
                })
            } else {
                await users.findOneAndUpdate({_id: id}, {
                    ...req.body
                })
            }
        } else {
            await users.create({email, first_name, last_name, password, user_role: "USER"})
        }

        res.send({
            success: true,
            msg: 'Operation Success'
        })
    } catch (e) {
        console.log(e)
        res.send({
            success: false,
            msg: 'Something went wrong!'
        });
    }
});

/* Delete users listing. */
router.post('/delete', authenticate, async (req, res, next) => {
    try {
        const {userIds} = req.body
        if (userIds.length > 0) {
            userIds.map(async item => {
                if (!mongoose.Types.ObjectId.isValid(item)) {
                    return res.status(400).json({
                        success: false,
                        msg: 'Unable to find User!'
                    })
                } else {
                    await users.findOneAndDelete({_id: item})
                }
            })

        } else {
            new Error("Users not fount")
        }

        res.send({
            success: true,
            msg: 'Operation Success'
        })
    } catch (e) {
        console.log(e)
        res.send({
            success: false,
            msg: 'Something went wrong!'
        });
    }
});

router.get('/:userId', authenticate, async (req, res, next) => {
    try {
        const {userId} = req.params
        if (userId) {
            if (!mongoose.Types.ObjectId.isValid(userId)) {
                return res.status(400).json({
                    success: false,
                    msg: 'Unable to find User!'
                })
            } else {
                const user = await users.findById(userId)
                res.send({
                    success: true,
                    body: JSON.parse(JSON.stringify(user))
                })
            }

        } else {
            new Error("Users not fount")
        }
    } catch (e) {
        console.log(e)
        res.send({
            success: false,
            msg: 'Something went wrong!'
        });
    }
});

module.exports = router;
