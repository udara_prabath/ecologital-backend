const router = require('express').Router();
const config = require('../config');
const users = require('../database/usersModel')
const jwt = require('jsonwebtoken');

router.post('/', async (req, res, next) => {
    const b64auth = (req.headers.authorization || '').split(' ')[1] || ''
    const [userName, basicPW] = Buffer.from(b64auth, 'base64').toString().split(':')

    if (userName && basicPW && userName === config.userName && basicPW === config.password) {
        try {
            const {email, password} = req.body;
            let user = JSON.parse(JSON.stringify(await users.find({email: email})))[0]
            if (user && user.password === password) {
                let accessToken = jwt.sign({
                    id: user._id,
                    logoutTimestamp: user.createdAt,
                    role: user.user_role
                }, config.SECRET, {
                    expiresIn: '60d'
                });
                res.send({
                    success: true,
                    body: {
                        accessToken,
                        user: {...user, password: undefined}
                    }
                });
            } else {
                res.send({
                    success: false,
                    message: 'Invalid username or password!'
                });
            }

        } catch (e) {
            res.send({
                success: false,
                message: 'Something went wrong!'
            });
        }

    } else {
        res.status(401).send({
            success: false,
            msg: 'Authentication Required!'
        })
    }
});

module.exports = router;
