const jwt = require('jsonwebtoken');
const config = require('../config');
const User = require('../database/usersModel');

const authenticate = async (req, res, next) => {
    const token = req.headers.authorization;
    if (!token) {
        return res.status(401).send({
            success: 'false',
            message: 'Authentication failed'
        });
    }

    try {
        const decoded = jwt.verify(token.replace('Bearer ', ''), config.SECRET);

        const {id, logoutTimestamp, role} = decoded;

        switch (role.toUpperCase()) {
            case 'ADMIN': {
                try{
                    await User.find({_id: id, createdAt: logoutTimestamp});
                    next();
                } catch (e) {
                    return res.status(401).send({
                        success: false,
                        message: 'Authentication failed'
                    });
                }
            } break;
            case 'USER': {
                try{
                    await User.find({_id: id, createdAt: logoutTimestamp});
                    next();
                } catch (e) {
                    return res.status(401).send({
                        success: false,
                        message: 'Authentication failed'
                    });
                }
            } break;
        }
    } catch (e) {
        return res.status(401).json({
            success: false,
            message: 'Authentication failed'
        });
    }
};

module.exports = authenticate;
